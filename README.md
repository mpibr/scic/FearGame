# FearGame
Qt/C++ state machine to drive fear conditioning with sound stimuli and video acquisition

## Configuration
Experimental configuration is done by an [INI](https://en.wikipedia.org/wiki/INI_file) file.

## UI
User interface is implemented in C++/Qt. The application provides a minimal user control to:

* choose a configuration file
* start / stop
* pause / resume


## Arduino engine
Provides low level timing of executing digital pulses. Communication is done via serial commands.

Classes:

* DriverSerial :: provides non-blocking serial message buffer
* DigitalPulse :: provides non-blocking digital pulse
* EventHandler :: provides interrupt safe event que
* HardwareLayout :: specific configuration based on FearGame setup, inherits from EventHandler

### Experimental Layout
The experimental layout include specific signals encoding experimental stimuli.

|Pins|Variable         |Description                        |
|----|-----------------|-----------------------------------|
|2   |PIN_I_SYNC_Basler|frame acquired signal nVista camera|
|3   |PIN_I_SYNC_nVista|frame acquired signal Basler camera|
|4   |PIN_O_TRIG_Basler|trigger Basler camera frame        |
|5   |PIN_O_TRIG_nVista|trigger nVista camera              |
|6   |PIN_O_TRIG_TDT   |trigger TDT sound system           |
|7   |PIN_O_TRIG_SHOCK |trigger shock                      |
|8   |PIN_O_VLTG_Basler|power voltage Basler camera        |
|9   |PIN_O_STOP_Basler|stop acquisition Basler camera     |

## List days per commits
```
git log --all|grep Date|cut -d " " -f4,5,6,8|awk -F"\t" 'OFS="\t"{LIST[$1]++}END{for(key in LIST) print key,LIST[key]}'
```