#include "socketclient.h"

SocketClient::SocketClient(QObject *parent) : QObject(parent),
  m_socket(new QTcpSocket(this))
{
    connect(m_socket, &QTcpSocket::connected, this, &SocketClient::on_connected);
    connect(m_socket, &QTcpSocket::disconnected, this, &SocketClient::on_disconnected);
    connect(m_socket, &QTcpSocket::readyRead, this, &SocketClient::on_readyRead);
    //connect(socket, &QTcpSocket::bytesWritten, this, &SocketClient::on_bytesWritten);
}


SocketClient::~SocketClient()
{
    if (m_socket->isOpen())
        m_socket->disconnectFromHost();
}


void SocketClient::on_enable(bool state)
{
    if (state) {
        m_socket->connectToHost(QHostAddress::LocalHost, 20001);
        if (!m_socket->waitForConnected(30000)) {
            emit log("red", "SocketClient :: Error " + m_socket->errorString());
        }
    }
    else {
        if (m_socket->isOpen()) {
            m_socket->disconnectFromHost();
        }
    }

}


void SocketClient::on_stop()
{
    quint64 packetSize = sizeof(quint64) + sizeof(quint32) + sizeof(quint64);
    quint32 packetType = SOCKET_STOP; // STOP
    quint64 packetData = static_cast<quint64>(SOCKET_NULL); // EMPTY
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_9);
    out << packetSize;
    out << packetType;
    out << packetData;
    if (m_socket->isValid())
        m_socket->write(block);
}


void SocketClient::on_connected()
{
    emit log("black","SocketClient :: connected");
}


void SocketClient::on_disconnected()
{
    emit log("black","SocketClient :: disconnected");
}


void SocketClient::on_readyRead()
{
    QByteArray block;

    if (m_socket->isValid()) {
        block = m_socket->readAll();
    }
    else {
        return;
    }

    QDataStream stream(&block, QIODevice::ReadOnly);
    quint64 packetSize;
    quint32 packetType;

    stream >> packetSize;
    stream >> packetType;

    switch (packetType) {
    case SOCKET_START :
        emit log("gray","SocketClient :: camera start");
        break;
    case SOCKET_STOP :
        emit log("gray","SocketClient :: camera stop");
        break;
    case SOCKET_FILENAME :
        emit log("gray","SocketClient :: filename");
        break;
    }
}

/*
void SocketClient::on_bytesWritten(qint64 bytes)
{
    qDebug() << "wrote: " << bytes;
}
*/
