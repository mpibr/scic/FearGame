#include "protocol.h"


void Protocol::parse(const QSettings &m_settings)
{
    // reinitialise
    trials.clear();
    triggers.clear();
    interTrialIntervals.clear();
    trialOrderIndex.clear();
    status.clear();

    parseSettings_protocol(m_settings);
    parseSettings_trials(m_settings);
    parseSettings_triggers(m_settings);

    generateInterTrialInterval();
    generateTrialOrderIndex();
}


int Protocol::duration()
{
    int durationMSec = 0;

    QVectorIterator<Trial *> iteratorTrials(trials);
    while (iteratorTrials.hasNext()) {
        Trial *nextTrial = iteratorTrials.next();
        durationMSec += nextTrial->repeats * nextTrial->duration(triggers);
    }

    QVectorIterator<int> iteratorIntervals(interTrialIntervals);
    while (iteratorIntervals.hasNext()) {
        durationMSec += iteratorIntervals.next();
    }

    return durationMSec;
}


void Protocol::parseSettings_protocol(const QSettings &settings)
{
    if (!settings.value("protocol").isNull()) {
        status = "Protocol :: configuration file is missing protocol settings";
        return;
    }

    name = settings.value("protocol/name", "Protocol").toString();
    path = settings.value("protocol/export", QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation)).toString();
    m_interTrialInterval_range = settings.value("protocol/interTrialIntervalRange", "[2000 5000]").toString();
    m_interTrialInterval_external = settings.value("protocol/interTrialIntervalExternal", "").toString();
    m_trialOrder = settings.value("protocol/trialOrder", "alternate").toString();
}


void Protocol::parseSettings_trials(const QSettings &settings)
{
    if (!settings.value("trial").isNull()) {
        status = "Protocol :: configuration file is missing trial settings";
        return;
    }

    int counter = 0;
    trialsCount = 0;
    QString key = "trial" + QString::number(counter);
    while (!settings.value(key + "/name").isNull()) {
        auto newTrial = new Trial();
        newTrial->name = settings.value(key + "/name", key).toString();
        newTrial->repeats = settings.value(key + "/repeats", 1).toInt();
        newTrial->external = settings.value(key + "/external", "").toString();
        if (!QFileInfo::exists(m_interTrialInterval_external) && !QFileInfo(m_interTrialInterval_external).isFile())
            status = "Protocol :: invalid external trial resource";
        newTrial->mask = static_cast<uint8_t>(settings.value(key + "/mask", 0).toUInt());
        trials.append(newTrial);
        trialsCount += newTrial->repeats;
        key = "trial" + QString::number(++counter);
    }
}


void Protocol::parseSettings_triggers(const QSettings &settings)
{
    if (!settings.value("trigger").isNull()) {
        status = "Protocol :: configuration file is missing trigger settings";
        return;
    }

    int counter = 0;
    QString key = "trigger" + QString::number(counter);
    while (!settings.value(key + "/name").isNull()) {
        auto newTrigger = new Trigger();
        newTrigger->name = settings.value(key + "/name", key).toString();
        newTrigger->line = static_cast<uint8_t>(settings.value(key + "/line", 2).toUInt());
        newTrigger->mask = static_cast<uint8_t>(settings.value(key + "/mask", 0).toUInt());
        newTrigger->delay = settings.value(key + "/delay", 0).toUInt();
        newTrigger->uptime = settings.value(key + "/uptime", 500).toUInt();
        newTrigger->downtime = settings.value(key + "/downtime", 500).toUInt();
        newTrigger->cycles = settings.value(key + "/cycles", 1).toUInt();
        triggers.append(newTrigger);
        key = "trigger" + QString::number(++counter);
    }
}


void Protocol::generateInterTrialInterval()
{
    m_interTrialInterval_range.remove('[');
    m_interTrialInterval_range.remove(']');
    QStringList itiRange = m_interTrialInterval_range.split(" ", QString::SkipEmptyParts);
    int itiRangeLow = itiRange.at(0).toInt(nullptr, 10);
    int itiRangeHigh = itiRange.at(1).toInt(nullptr, 10);
    for (int k = 0; k < (trialsCount + 1); k++) {
        int value = generateRandomValue(itiRangeLow, itiRangeHigh);
        interTrialIntervals.append(value);
    }

    // check if external file is required
    if (!QFileInfo::exists(m_interTrialInterval_external) && !QFileInfo(m_interTrialInterval_external).isFile())
        return;

    // parse external file
    QFile fileExternal(m_interTrialInterval_external);
    if (!fileExternal.open(QIODevice::ReadOnly)) {
        status = "Protocol :: failed to open external InterTrialInterval file " + m_interTrialInterval_external;
        return;
    }

    QTextStream fileHandle(&fileExternal);
    int lineCounter = 0;
    while (!fileHandle.atEnd()) {
        bool ok = false;
        QString fileLine = fileHandle.readLine();

        if (fileLine.startsWith('#'))
            continue;

        int value = fileLine.toInt(&ok, 10);
        if (!ok)
            continue;

        interTrialIntervals[lineCounter++] = value;

        if (lineCounter >= (trialsCount + 1))
            break;
    }

}


void Protocol::generateTrialOrderIndex()
{
    int index = 0;
    for (int k = 0; k < trialsCount; k++) {
        trialOrderIndex.append(index++);
        if (index >= trials.size())
            index = 0;
    }

    if (m_trialOrder.startsWith("sor", Qt::CaseInsensitive))
        std::sort(trialOrderIndex.begin(), trialOrderIndex.end());

    if (m_trialOrder.startsWith("ran", Qt::CaseInsensitive))
        std::shuffle(trialOrderIndex.begin(), trialOrderIndex.end(), m_rnd);
}


int Protocol::generateRandomValue(int low, int high)
{
    if (low > high)
        qSwap(low, high);

    return low + (std::rand() % (high - low + 1));
}
