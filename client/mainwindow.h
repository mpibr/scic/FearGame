#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QStandardPaths>
#include <QSerialPortInfo>
#include <QTimer>
#include <QDateTime>
#include <QSettings>


#include "enginedriver.h"
#include "serialport.h"
#include "protocol.h"
#include "eventwriter.h"
#include "tdtinterface.h"
#include "socketclient.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    EngineDriver *m_engine;
    SerialPort *m_serial;
    Protocol *m_protocol;
    EventWriter *m_writer;
    TDTInterface *m_tdt;
    SocketClient *m_client;
    QTimer *m_timerRun;
    QTimer *m_timerBaseline;
    QTimer *m_timerTrial;
    QTimer *m_timerWriterClose;
    int m_run_elapsedMSec;
    int m_index_iti;
    int m_index_trial;

    void event_stop();
    void event_tick();

signals:
    void log(const QString &color, const QString &message);

    void writerOpen(const QString &filePath, const QString &fileTag);

    void portConnect(const QString &port, int baudRate);
    void portDisconnect();

    void command(const QString &message);
    void handshake();
    void set(uint8_t mask, uint32_t delay, uint32_t timeUp, uint32_t timeDown, uint16_t cycles);
    void enable(uint8_t mask, uint8_t state);

    void socketEnable(bool state);
    void socketStopCamera();

private slots:
    void on_lineEdit_arduino_command_returnPressed();
    void on_pushButton_arduino_connect_toggled(bool checked);
    void on_pushButton_protocol_load_clicked();
    void on_pushButton_run_start_clicked();
    void on_pushButton_run_stop_clicked();

    void on_timeoutTimerRun();
    void on_timeoutTimerBaseline();
    void on_timeoutTimerTrial();
};

#endif // MAINWINDOW_H
