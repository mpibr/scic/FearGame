#include "tdtinterface.h"

int TDTInterface::connect(const QString &portName, int portId, int deviceId)
{
    // connect device
    if (deviceId == TDT_DEVICE_RP2) {
        rp.ConnectRP2(portName, portId);
    }
    else if (deviceId == TDT_DEVICE_RZ6) {
        rp.ConnectRZ6(portName, portId);
    }

    // check status
    if (checkStatus(TDT_STATUS_CONNECTED)) {

        if (checkStatus(TDT_STATUS_CIRCUIT_RUNNING))
            rp.Halt();

        if (checkStatus(TDT_STATUS_CIRCUIT_LOADED))
            rp.ClearCOF();
    }

    return rp.GetStatus();
}


int TDTInterface::load(const QString &fileRCX)
{
    if (checkStatus(TDT_STATUS_CONNECTED)) {

        if (checkStatus(TDT_STATUS_CIRCUIT_RUNNING))
            rp.Halt();

        if (checkStatus(TDT_STATUS_CIRCUIT_LOADED))
            rp.ClearCOF();

        rp.LoadCOF(fileRCX);
    }

    return rp.GetStatus();
}


int TDTInterface::run()
{
    if (checkStatus(TDT_STATUS_CONNECTED)) {

        if (checkStatus(TDT_STATUS_CIRCUIT_RUNNING)) {
            return 0;
        }

        if (!checkStatus(TDT_STATUS_CIRCUIT_LOADED)) {
            return 0;
        }

        rp.Run();
    }

    return rp.GetStatus();
}


int TDTInterface::soft()
{
    return rp.SoftTrg(1);
}


void TDTInterface::destroy()
{
    if (checkStatus(TDT_STATUS_CONNECTED)) {

        if (checkStatus(TDT_STATUS_CIRCUIT_RUNNING))
            rp.Halt();

        if (checkStatus(TDT_STATUS_CIRCUIT_LOADED))
            rp.ClearCOF();
    }

    rp.clear();
}


bool TDTInterface::checkStatus(int statusQuery)
{

    bool status = false;
    int statusReference = rp.GetStatus();
    if ((statusReference & statusQuery) == statusQuery)
        status = true;

    return status;
}
