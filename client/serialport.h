#ifndef SERIALPORT_H
#define SERIALPORT_H

#include <QObject>
#include <QSerialPort>
#include <QTimer>
#include <QQueue>
#include <QDebug>

class SerialPort : public QObject
{
    Q_OBJECT

public:
    explicit SerialPort(QObject *parent = nullptr);
    ~SerialPort();

private:
    bool m_flagWaiting;
    QSerialPort *m_serialPort;
    QTimer *m_timerRespond;
    QQueue<QByteArray> *m_queue;

    const qint16 TIMER_INTERVAL_RESPOND = 2000;

    void disconnectPort(const QString &message);

public slots:
    void on_connect(const QString &portName, int baudRate);
    void on_disconnect();
    void on_schedule(const QByteArray &package);

private slots:
    void on_bufferRead();
    void on_bufferWrite();

signals:
    void log(const QString &color, const QString &message);
    void readyRespond(const QByteArray &respond);
    void status(bool connected);
};

#endif // SERIALPORT_H
