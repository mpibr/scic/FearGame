#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_engine(new EngineDriver(this)),
    m_serial(new SerialPort(this)),
    m_protocol(new Protocol()),
    m_writer(new EventWriter(this)),
    m_tdt(new TDTInterface()),
    m_client(new SocketClient(this)),
    m_timerRun(new QTimer(this)),
    m_timerBaseline(new QTimer(this)),
    m_timerTrial(new QTimer(this)),
    m_timerWriterClose(new QTimer(this)),
    m_run_elapsedMSec(0),
    m_index_iti(0),
    m_index_trial(0)
{
    ui->setupUi(this);

    // add available ports to list
    foreach (const QSerialPortInfo &serialPortInfo, QSerialPortInfo::availablePorts()) {

        if (serialPortInfo.description().contains("Arduino"))
            ui->comboBox_arduino_port->addItem(serialPortInfo.portName());
    }

    connect(this, &MainWindow::log, ui->widget_board, &EventLog::on_log);

    connect(this, &MainWindow::writerOpen, m_writer, &EventWriter::on_open);
    connect(m_timerWriterClose, &QTimer::timeout, m_writer, &EventWriter::on_close);
    m_timerWriterClose->setSingleShot(true);
    connect(m_engine, &EngineDriver::write, m_writer, &EventWriter::on_write);

    connect(m_engine, &EngineDriver::log, this, &MainWindow::log);
    connect(this, &MainWindow::command, m_engine, &EngineDriver::on_command);
    connect(this, &MainWindow::handshake, m_engine, &EngineDriver::on_handshake);
    connect(this, &MainWindow::set, m_engine, &EngineDriver::on_set);
    connect(this, &MainWindow::enable, m_engine, &EngineDriver::on_enable);

    // serial port
    connect(this, &MainWindow::portConnect, m_serial, &SerialPort::on_connect);
    connect(this, &MainWindow::portDisconnect, m_serial, &SerialPort::on_disconnect);
    connect(m_engine, &EngineDriver::schedule, m_serial, &SerialPort::on_schedule);
    connect(m_serial, &SerialPort::log, m_engine, &EngineDriver::log);
    connect(m_serial, &SerialPort::readyRespond, m_engine, &EngineDriver::on_respond);

    // timers
    connect(m_timerRun, &QTimer::timeout, this, &MainWindow::on_timeoutTimerRun);
    connect(m_timerBaseline, &QTimer::timeout, this, &MainWindow::on_timeoutTimerBaseline);
    connect(m_timerTrial, &QTimer::timeout, this, &MainWindow::on_timeoutTimerTrial);
    m_timerBaseline->setSingleShot(true);
    m_timerTrial->setSingleShot(true);

    // socket
    connect(m_client, &SocketClient::log, this, &MainWindow::log);
    connect(this, &MainWindow::socketEnable, m_client, &SocketClient::on_enable);
    connect(this, &MainWindow::socketStopCamera, m_client, &SocketClient::on_stop);
}

MainWindow::~MainWindow()
{
    m_tdt->destroy();
    delete m_tdt;
    delete m_protocol;
    delete ui;
}

void MainWindow::on_lineEdit_arduino_command_returnPressed()
{
    emit command(ui->lineEdit_arduino_command->text().toUtf8());
}


void MainWindow::on_pushButton_protocol_load_clicked()
{
    // load ini file
    QString fileName_config = QFileDialog::getOpenFileName(this,
                                                           tr("Open Settings File"),
                                                           QStandardPaths::writableLocation(QStandardPaths::DesktopLocation),
                                                           tr("FearGame settings (*.txt)"));
    if (fileName_config.isEmpty()) {
        emit log("red", "MainWindow :: load protocol failed, please choose a valid INI file");
        return;
    }

    // check file validity
    if (!QFileInfo::exists(fileName_config) && !QFileInfo(fileName_config).isFile()) {
        emit log("red", "EngineDriver :: configuration file does not exist " + fileName_config);
        return;
    }

    // read configuration file
    QSettings settings(fileName_config, QSettings::IniFormat, this);
    m_protocol->parse(settings);
    if (!m_protocol->status.isEmpty()) {
        emit log("red", m_protocol->status);
        return;
    }

    ui->lineEdit_protocol_name->setText(m_protocol->name);
    ui->lineEdit_protocol_duration->setText(QDateTime::fromMSecsSinceEpoch(m_protocol->duration()).toUTC().toString("hh:mm:ss"));
    ui->lineEdit_protocol_trials->setText(QString::number(m_protocol->trialsCount));
}


void MainWindow::on_pushButton_arduino_connect_toggled(bool checked)
{
    emit socketEnable(checked);

    if (checked) {
        QString portName = ui->comboBox_arduino_port->currentText();
        int portBaudRate = ui->comboBox_arduino_baudRate->currentText().toInt();
        emit portConnect(portName, portBaudRate);
        ui->lineEdit_arduino_status->setText("connected");
        ui->lineEdit_arduino_status->setStyleSheet("color: #00FF00");
    }
    else {
        emit portDisconnect();
        ui->lineEdit_arduino_status->setText("disconnected");
        ui->lineEdit_arduino_status->setStyleSheet("color: #FF0000");
    }
}


void MainWindow::on_pushButton_run_start_clicked()
{
    // re-set counters
    m_run_elapsedMSec = m_protocol->duration();
    m_index_iti = 0;
    m_index_trial = 0;

    // open writer
    emit writerOpen(m_protocol->path, m_protocol->name);

    // check connection
    emit handshake();

    // configure triggers
    QVectorIterator<Trigger *> iter(m_protocol->triggers);
    while (iter.hasNext()) {

        Trigger *trig = iter.next();
        emit set(trig->mask, trig->delay, trig->uptime, trig->downtime, trig->cycles);
    }

    // enable camera
    emit enable(m_protocol->triggers[0]->mask, 0x01);

    // trigger timers
    m_timerRun->start(1000);
    m_timerBaseline->start(m_protocol->interTrialIntervals.at(m_index_iti++));

    // update ui context
    event_tick();
    ui->lineEdit_run_state->setText("Run: Baseline");
    ui->pushButton_run_start->setEnabled(false);
    ui->pushButton_protocol_load->setEnabled(false);

    // TDT
    if ((m_tdt->connect("GB", 1, TDTInterface::TDT_DEVICE_RZ6) & TDTInterface::TDT_STATUS_CONNECTED) == TDTInterface::TDT_STATUS_CONNECTED) {
        emit log("black", "TDTInterface :: RZ6 is connected on GB");
    }
    else {
        emit log("red", "TDTInterface :: RZ6 failed to connect on GB");
    }

}


void MainWindow::on_pushButton_run_stop_clicked()
{
    event_stop();
}


void MainWindow::on_timeoutTimerRun()
{
    m_run_elapsedMSec -= 1000;
    if (m_run_elapsedMSec <= 0) {
        event_stop();
    }
    else {
        event_tick();
    }
}


void MainWindow::on_timeoutTimerBaseline()
{
    // on baseline timeout we start next trial
    if (m_protocol->trialOrderIndex.size() <= m_index_trial)
        return;

    int nextTrial_index = m_protocol->trialOrderIndex[m_index_trial++];
    Trial *nextTrial = m_protocol->trials[nextTrial_index];
    int nextTrial_duration = nextTrial->duration(m_protocol->triggers);
    emit enable(nextTrial->mask, 0x01);
    m_timerTrial->start(nextTrial_duration);
    ui->lineEdit_run_state->setText(nextTrial->name);

    if ((m_tdt->load(nextTrial->external) & TDTInterface::TDT_STATUS_CIRCUIT_LOADED) == TDTInterface::TDT_STATUS_CIRCUIT_LOADED) {
        emit log("gray", "TDTInterface :: circuit is loaded " + nextTrial->external);
    }
    else {
        emit log("red", "TDTInterface :: circuit failed to load " + nextTrial->external);
    }


    if ((m_tdt->run() & TDTInterface::TDT_STATUS_CIRCUIT_RUNNING) == TDTInterface::TDT_STATUS_CIRCUIT_RUNNING) {
        emit log("gray", "TDTInterface :: circuit is running");
        m_tdt->soft();
    }
    else {
        emit log("red", "TDTInterface :: cicuit failed to run");
    }
}


void MainWindow::on_timeoutTimerTrial()
{
    // on trial timeout we start next inter trial interval
    if (m_protocol->interTrialIntervals.size() <= m_index_iti)
        return;
    int nextIti_duraiton = m_protocol->interTrialIntervals.at(m_index_iti++);

    m_timerBaseline->start(nextIti_duraiton);
    ui->lineEdit_run_state->setText("Baseline");
}


void MainWindow::event_stop()
{


    uint8_t mask = 0;
    QVectorIterator<Trigger *> iter(m_protocol->triggers);
    while (iter.hasNext()) {

        Trigger *trig = iter.next();
        mask |= trig->mask;
    }

    emit enable(mask, 0x00);
    ui->pushButton_run_start->setEnabled(true);
    ui->pushButton_protocol_load->setEnabled(true);
    ui->lineEdit_run_state->setText("Idle");
    m_timerRun->stop();
    m_timerBaseline->stop();
    m_timerTrial->stop();
    m_timerWriterClose->start(500);
    emit socketStopCamera();
}


void MainWindow::event_tick()
{
    QString elapsedTime = QDateTime::fromMSecsSinceEpoch(m_run_elapsedMSec).toUTC().toString("hh:mm:ss");
    ui->lineEdit_run_elapsed->setText(elapsedTime);
}
