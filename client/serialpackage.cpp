#include "serialpackage.h"

SerialPackage::SerialPackage()
{

}


QString SerialPackage::parse(const QString &message)
{
    QString buffer = message;

    // clean message
    buffer.remove(QRegularExpression("[^A-Fa-f0-9]"));

    return buffer.toUpper();
}


QString SerialPackage::package(const QString &buffer)
{
    quint8 checksumValue = checksum(buffer);
    QString checksumText = pack(checksumValue);
    return '!' + buffer + checksumText + '\n';
}


bool SerialPackage::isSizeModular(const QString &buffer)
{
    return (buffer.size() % 2) == 0;
}


bool SerialPackage::isSizeValid(const QString &buffer, uint16_t bufferSize)
{
    return buffer.size() == bufferSize;
}


bool SerialPackage::isChecksumValid(const QString &buffer)
{
    // check received checksum
    uint8_t checksumReceived = 0;
    unpack(&checksumReceived, buffer.mid(buffer.size() - 2, 2));

    return (checksumReceived == checksum(buffer.mid(0, buffer.size() - 2)));
}


uint8_t SerialPackage::checksum(const QString &buffer)
{
    uint8_t valueChecksum = 0;

    for (int i = 0; i < buffer.size(); i += 2) {
        uint8_t value = 0;
        unpack(&value, buffer.mid(i, 2));
        valueChecksum += value;
    }

    valueChecksum = ~valueChecksum + 1;

    return valueChecksum;
}


QString SerialPackage::pack(uint8_t value)
{
    return QString("%1").arg(value, 2, 16, QChar('0')).toUpper();
}


QString SerialPackage::pack(uint16_t value)
{
    return QString("%1").arg(value, 4, 16, QChar('0')).toUpper();
}


QString SerialPackage::pack(uint32_t value)
{
    return QString("%1").arg(value, 8, 16, QChar('0')).toUpper();
}


void SerialPackage::unpack(uint8_t *value, const QString &textValue)
{
    *value = static_cast<uint8_t>(textValue.toUInt(nullptr, 16));
}


void SerialPackage::unpack(uint16_t *value, const QString &textValue)
{
    *value = static_cast<uint16_t>(textValue.toUInt(nullptr, 16));
}


void SerialPackage::unpack(uint32_t *value, const QString &textValue)
{
    *value = static_cast<uint32_t>(textValue.toUInt(nullptr, 16));
}
