#ifndef EVENTWRITER_H
#define EVENTWRITER_H

#include <QObject>
#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QDateTime>
#include <QDebug>

class EventWriter : public QObject
{
    Q_OBJECT

public:
    explicit EventWriter(QObject *parent = nullptr);
    ~EventWriter();

private:
    QFile *m_logFile;
    QTextStream *m_logStream;

public slots:
    void on_open(const QString &filePath, const QString &fileTag);
    void on_write(const QString &message);
    void on_close();
};

#endif // EVENTWRITER_H
