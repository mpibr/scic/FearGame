#include "enginedriver.h"

EngineDriver::EngineDriver(QObject *parent) : QObject(parent)
{

}


void EngineDriver::on_command(const QString &message)
{
    emit schedule(SerialPackage::package(message).toUtf8());
}


void EngineDriver::on_respond(const QByteArray &respond)
{
    uint8_t address = 0;
    uint8_t command = 0;
    uint32_t counter = 0;
    uint32_t timestamp = 0;

    QString buffer = SerialPackage::parse(QString(respond));

    if (!SerialPackage::isSizeModular(buffer))
        emit log("red", "EngineDriver :: message is not modular");

    if (!SerialPackage::isChecksumValid(buffer))
        emit log("red", "EngineDriver :: respond checksum failed");

    SerialPackage::unpack(&address, buffer.mid(0, 2));
    SerialPackage::unpack(&command, buffer.mid(2, 2));
    SerialPackage::unpack(&counter, buffer.mid(4, 8));
    SerialPackage::unpack(&timestamp, buffer.mid(12, 8));

    if (address != ADDRESS_ENGINE_CLIENT)
        emit log("red", "EngineDriver :: wrong address");

    switch (command) {
    case CODE_HANDSHAKE:
        event_handshake(counter, timestamp, buffer);
        break;
    case CODE_SET:
        event_set(counter, timestamp, buffer);
        break;
    case CODE_ENABLE:
        event_enable(counter, timestamp, buffer);
        break;
    case CODE_PORT:
        event_port(counter, timestamp, buffer);
        break;
    case CODE_ERROR:
        event_error(counter, timestamp, buffer);
        break;
    default:
        emit log("red", "EngineDriver :: unknown command");
        break;
    }
}


void EngineDriver::on_handshake()
{
    QString command = SerialPackage::pack(ADDRESS_CLIENT_ENGINE) +
                      SerialPackage::pack(CODE_HANDSHAKE);
    emit schedule(SerialPackage::package(command).toUtf8());
}


void EngineDriver::on_set(uint8_t mask, uint32_t delay, uint32_t timeUp, uint32_t timeDown, uint16_t cycles)
{
    QString command = SerialPackage::pack(ADDRESS_CLIENT_ENGINE) +
                      SerialPackage::pack(CODE_SET) +
                      SerialPackage::pack(mask) +
                      SerialPackage::pack(delay) +
                      SerialPackage::pack(timeUp) +
                      SerialPackage::pack(timeDown) +
                      SerialPackage::pack(cycles);
    emit schedule(SerialPackage::package(command).toUtf8());
}


void EngineDriver::on_enable(uint8_t mask, uint8_t state)
{
    QString command = SerialPackage::pack(ADDRESS_CLIENT_ENGINE) +
                      SerialPackage::pack(CODE_ENABLE) +
                      SerialPackage::pack(mask) +
                      SerialPackage::pack(state);
    emit schedule(SerialPackage::package(command).toUtf8());
}


void EngineDriver::event_handshake(uint32_t counter, uint32_t timestamp, const QString &buffer)
{
    QString message = QString("HANDSHAKE %1 %2 %3").arg(counter).arg(timestamp).arg(buffer.mid(20, 4));
    emit log("gray", message);
    emit write(message);
}


void EngineDriver::event_set(uint32_t counter, uint32_t timestamp, const QString &buffer)
{
    uint8_t mask = 0;
    uint32_t delay = 0;
    uint32_t uptime = 0;
    uint32_t downtime = 0;
    uint16_t cycles = 0;
    SerialPackage::unpack(&mask, buffer.mid(20, 2));
    SerialPackage::unpack(&delay, buffer.mid(22, 8));
    SerialPackage::unpack(&uptime, buffer.mid(30, 8));
    SerialPackage::unpack(&downtime, buffer.mid(38, 8));
    SerialPackage::unpack(&cycles, buffer.mid(46, 4));
    QString message = QString("SETTRIGGER %1 %2 mask=%3,delay=%4,uptime=%5,downtime=%6,cycles=%7").arg(counter).arg(timestamp).arg(mask).arg(delay).arg(uptime).arg(downtime).arg(cycles);
    emit log("gray", message);
    emit write(message);
}


void EngineDriver::event_enable(uint32_t counter, uint32_t timestamp, const QString &buffer)
{
    uint8_t mask = 0;
    uint8_t state = 0;
    SerialPackage::unpack(&mask, buffer.mid(20, 2));
    SerialPackage::unpack(&state, buffer.mid(22, 2));
    QString message = QString("ENABLE %1 %2 mask=%3,state=%4").arg(counter).arg(timestamp).arg(mask).arg(state);
    emit log("gray", message);
    emit write(message);
}


void EngineDriver::event_port(uint32_t counter, uint32_t timestamp, const QString &buffer)
{
    uint8_t port = 0;
    SerialPackage::unpack(&port, buffer.mid(20, 2));
    QString message = QString("PORT %1 %2 %3").arg(counter).arg(timestamp).arg(port);
    emit write(message);
}


void EngineDriver::event_error(uint32_t counter, uint32_t timestamp, const QString &buffer)
{
    uint8_t errorId = 0;
    SerialPackage::unpack(&errorId, buffer.mid(20, 2));
    QString errorMessage = "unknown";

    switch(errorId) {
    case ERROR_INVALID_ADDRESS:
        errorMessage = "invalid address";
        break;
    case ERROR_INVALID_CALLBACK:
        errorMessage = "invalid callback";
        break;
    case ERROR_INVALID_CHECKSUM:
        errorMessage = "invalid checksum";
        break;
    case ERROR_INVALID_CODE:
        errorMessage = "invalid code";
        break;
    case ERROR_MESSAGE_ISSHORT:
        errorMessage = "message is short";
        break;
    case ERROR_MESSAGE_TRUNCATED:
        errorMessage = "message is truncated";
        break;
    case ERROR_TRIGGER_ENABLE:
        errorMessage = "failed trigger enable";
        break;
    case ERROR_TRIGGER_SET:
        errorMessage = "failed trigger set";
        break;
    }

    QString message = QString("ERROR %1 %2 %3").arg(counter).arg(timestamp).arg(errorMessage);
    emit log("red", message);
    emit write(message);
}
