#ifndef SOCKETCLIENT_H
#define SOCKETCLIENT_H

#include <QObject>
#include <QHostAddress>
#include <QTcpSocket>
#include <QDataStream>

class SocketClient : public QObject
{
    Q_OBJECT
public:
    explicit SocketClient(QObject *parent = nullptr);
    ~SocketClient();

signals:
    void log(const QString &color, const QString &message);

public slots:
    void on_enable(bool state);
    void on_stop();

private slots:
    void on_connected();
    void on_disconnected();
    void on_readyRead();
    //void on_bytesWritten(qint64 bytes);

private:
    QTcpSocket *m_socket;
    static const quint32 SOCKET_NULL = 0;
    static const quint32 SOCKET_START = 1;
    static const quint32 SOCKET_STOP = 2;
    static const quint32 SOCKET_FILENAME = 3;
};

#endif // SOCKETCLIENT_H
