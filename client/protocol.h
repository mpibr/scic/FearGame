#ifndef PROTOCOL_H
#define PROTOCOL_H

#include <QObject>
#include <QVector>
#include <QSettings>
#include <QFileInfo>
#include <QStandardPaths>
#include <QDebug>
#include <algorithm>
#include <random>

class Trigger
{
public:
    explicit Trigger() :
        line(0),
        mask(0),
        delay(0),
        uptime(500),
        downtime(500),
        cycles(1),
        name("Trigger"){}

    uint8_t line;
    uint8_t mask;
    uint32_t delay;
    uint32_t uptime;
    uint32_t downtime;
    uint16_t cycles;
    QString name;
    int32_t duration(){return static_cast<int32_t>(delay + cycles * (uptime + downtime));}
};


class Trial
{
public:
    explicit Trial() : mask(0), repeats(1), name("Trial"), external("") {}

    uint8_t mask;
    uint16_t repeats;
    QString name;
    QString external;

    int32_t duration(const QVector<Trigger *> &m_triggers) {
        int32_t value = 0;
        QVectorIterator<Trigger *> iter(m_triggers);
        while (iter.hasNext()) {
            Trigger *triggerNow = iter.next();

            if (mask & triggerNow->mask)
                value = qMax(value, triggerNow->duration());
        }
        return value;
    }

};


class Protocol
{
public:
    explicit Protocol() {}

    void parse(const QSettings &m_settings);
    int duration();

    int trialsCount;
    QString name;
    QString path;
    QString status;

    QVector <Trigger *> triggers;
    QVector<Trial *> trials;
    QVector<int> interTrialIntervals;
    QVector<int> trialOrderIndex;

private:
    QString m_interTrialInterval_range;
    QString m_interTrialInterval_external;
    QString m_trialOrder;
    std::default_random_engine m_rnd;

    void parseSettings_protocol(const QSettings &settings);
    void parseSettings_trials(const QSettings &settings);
    void parseSettings_triggers(const QSettings &settings);

    void generateInterTrialInterval();
    void generateTrialOrderIndex();
    static int generateRandomValue(int low, int high);
};

#endif // PROTOCOL_H
