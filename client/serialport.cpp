#include "serialport.h"

SerialPort::SerialPort(QObject *parent) : QObject(parent),
    m_flagWaiting(false),
    m_serialPort(new QSerialPort(this)),
    m_timerRespond(new QTimer(this)),
    m_queue(new QQueue<QByteArray>())
{
    // configure respond timer
    m_timerRespond->setSingleShot(true);
    m_timerRespond->setInterval(TIMER_INTERVAL_RESPOND);
    connect(m_timerRespond, &QTimer::timeout, [this](){emit log("red", "SerialPort :: respond timeout");});
    connect(m_serialPort, &QSerialPort::readyRead, this, &SerialPort::on_bufferRead);
}

SerialPort::~SerialPort()
{
    disconnectPort("SerialPort :: quit");

    delete m_queue;
}


void SerialPort::on_connect(const QString &portName, int baudRate)
{
    // configure port
    m_serialPort->setPortName(portName);
    m_serialPort->setBaudRate(baudRate);
    m_serialPort->setDataBits(QSerialPort::Data8);
    m_serialPort->setParity(QSerialPort::NoParity);
    m_serialPort->setFlowControl(QSerialPort::NoFlowControl);
    m_serialPort->setStopBits(QSerialPort::OneStop);
    m_serialPort->open(QIODevice::ReadWrite);
    if (!m_serialPort->isOpen())
    {
        disconnectPort("SerialPort :: failed to open serial port " + m_serialPort->portName());
    }

    emit log("black",
             "SerialPort :: device connected @ port=" + m_serialPort->portName() + ", baudRate=" + QString::number(m_serialPort->baudRate()));
    emit status(true);

}


void SerialPort::on_disconnect()
{
    disconnectPort("SerialPort :: device disconnected");
}


void SerialPort::on_schedule(const QByteArray &package)
{
    m_queue->enqueue(package);
    on_bufferWrite();
}


void SerialPort::on_bufferRead()
{
    if (!m_serialPort->canReadLine())
        return;

    // emit respond message
    m_timerRespond->stop();
    emit readyRespond(m_serialPort->readLine());

    // check if new message is waiting
    m_flagWaiting = false;
    on_bufferWrite();
}


void SerialPort::on_bufferWrite()
{
    if (!m_serialPort->isOpen())
        return;

    if (m_flagWaiting)
        return;

    if (m_queue->isEmpty())
        return;

    QByteArray package = m_queue->dequeue();
    qint64 bytesWritten = m_serialPort->write(package);

    // check transfer
    if (bytesWritten == -1)
    {
        emit log("red", "SerialPort :: failed to write data to serial port.");
    }
    else if (bytesWritten != package.size())
    {
        emit log("red", "SerialPort :: serial port sent a truncated message.");
    }
    else
    {
        m_timerRespond->start();
        QString message = QString::fromUtf8(package);
        emit log("gray", "SerialPort :: sending " + message.trimmed());
        m_flagWaiting = true;
    }
}


void SerialPort::disconnectPort(const QString &message)
{
    m_flagWaiting = false;
    m_queue->clear();

    if (m_timerRespond->isActive())
        m_timerRespond->isActive();

    if (m_serialPort->isOpen())
        m_serialPort->close();

    emit status(false);
    emit log("red", message);
}
