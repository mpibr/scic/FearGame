#ifndef ENGINEDRIVER_H
#define ENGINEDRIVER_H

#include <QObject>
#include <QDebug>

#include "serialpackage.h"

class EngineDriver : public QObject
{
    Q_OBJECT
public:
    explicit EngineDriver(QObject *parent = nullptr);

private:

    // ADDRESS SPACE
    static const uint8_t ADDRESS_CLIENT_ENGINE = 0xCE;
    static const uint8_t ADDRESS_ENGINE_CLIENT = 0xEC;

    // CODES
    static const uint8_t CODE_HANDSHAKE = 0x00;
    static const uint8_t CODE_SET = 0x01;
    static const uint8_t CODE_ENABLE = 0x02;
    static const uint8_t CODE_PORT = 0xAA;

    // ERROR HANDLING
    static const uint8_t CODE_ERROR = 0xEE;
    static const uint8_t ERROR_MESSAGE_TRUNCATED = 0xE0;
    static const uint8_t ERROR_INVALID_CHECKSUM = 0xE1;
    static const uint8_t ERROR_INVALID_ADDRESS = 0xE2;
    static const uint8_t ERROR_INVALID_CODE = 0xE3;
    static const uint8_t ERROR_INVALID_CALLBACK = 0xE4;
    static const uint8_t ERROR_MESSAGE_ISSHORT = 0xE5;
    static const uint8_t ERROR_TRIGGER_SET = 0xE6;
    static const uint8_t ERROR_TRIGGER_ENABLE = 0xE7;

    void event_handshake(uint32_t counter, uint32_t timestamp, const QString &buffer);
    void event_set(uint32_t counter, uint32_t timestamp, const QString &buffer);
    void event_enable(uint32_t counter, uint32_t timestamp, const QString &buffer);
    void event_port(uint32_t counter, uint32_t timestamp, const QString &buffer);
    void event_error(uint32_t counter, uint32_t timestamp, const QString &buffer);

signals:
    void log(const QString &color, const QString &message);
    void write(const QString &message);
    void schedule(const QByteArray &package);

public slots:
    void on_command(const QString &message);
    void on_respond(const QByteArray &respond);
    void on_handshake();
    void on_set(uint8_t mask, uint32_t delay, uint32_t timeUp, uint32_t timeDown, uint16_t cycles);
    void on_enable(uint8_t mask, uint8_t state);
};

#endif // ENGINEDRIVER_H
