#ifndef SERIALPACKAGE_H
#define SERIALPACKAGE_H

#include <QString>
#include <QRegularExpression>

class SerialPackage
{
public:
    explicit SerialPackage();

    static QString parse(const QString &message);
    static QString package(const QString &buffer);

    static bool isSizeModular(const QString &buffer);
    static bool isSizeValid(const QString &buffer, uint16_t bufferSize);
    static bool isChecksumValid(const QString &buffer);

    static uint8_t checksum(const QString &buffer);

    static QString pack(uint8_t value);
    static QString pack(uint16_t value);
    static QString pack(uint32_t value);

    static void unpack(uint8_t *value, const QString &textValue);
    static void unpack(uint16_t *value, const QString &textValue);
    static void unpack(uint32_t *value, const QString &textValue);
};

#endif // SERIALPACKAGE_H
