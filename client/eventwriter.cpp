#include "eventwriter.h"

EventWriter::EventWriter(QObject *parent) : QObject(parent),
    m_logFile(nullptr),
    m_logStream(nullptr)
{

}


EventWriter::~EventWriter()
{
    if (m_logFile != nullptr) {
        if (m_logFile->isOpen())
            m_logFile->close();
        delete m_logFile;
    }

    if (m_logStream != nullptr)
        delete m_logStream;
}

void EventWriter::on_open(const QString &filePath, const QString &fileTag)
{
    QString fileTimestamp = QDateTime::currentDateTime().toString("yyyy-MM-dd-hh-mm-ss");
    QString fileName = QString("cam0_%1_%2.txt").arg(fileTimestamp).arg(fileTag);
    QDir pathExport = QDir(filePath);

    // check path
    if (!pathExport.exists())
        pathExport.mkdir(filePath);

    // open file for writing
    if (m_logFile != nullptr) {
        if (m_logFile->isOpen())
            m_logFile->close();
        delete m_logFile;
    }

    if (m_logStream != nullptr)
        delete m_logStream;

    m_logFile = new QFile(pathExport.filePath(fileName));
    m_logFile->open(QIODevice::WriteOnly | QIODevice::Text);
    m_logStream = new QTextStream(m_logFile);
}


void EventWriter::on_write(const QString &message)
{
    // write to fle
    if (m_logFile->isOpen()) {
        if (m_logStream != nullptr)
            *m_logStream << message << "\n";
    }
}


void EventWriter::on_close()
{
    if (m_logFile != nullptr) {
        if (m_logFile->isOpen())
            m_logFile->close();
    }
}
