#ifndef EVENTLOG_H
#define EVENTLOG_H

#include <QObject>
#include <QWidget>
#include <QPlainTextEdit>
#include <QScrollBar>
#include <QDateTime>

class EventLog : public QPlainTextEdit
{
    Q_OBJECT

public:
    explicit EventLog(QWidget *parent = nullptr);

private:
    bool m_useTimestamp;
    void addMessageToBoard(const QString &message, QBrush brush);

public slots:
    void on_log(const QString &color, const QString &message);
    void on_setTimestamp(bool state);
};

#endif // EVENTLOG_H
