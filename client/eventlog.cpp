#include "eventlog.h"

EventLog::EventLog(QWidget *parent) : QPlainTextEdit (parent),
    m_useTimestamp(true)
{

}


void EventLog::on_setTimestamp(bool state)
{
    m_useTimestamp = state;
}


void EventLog::addMessageToBoard(const QString &message, QBrush brush)
{
    QTextCharFormat textFormat = this->currentCharFormat();
    textFormat.setForeground(brush);
    //this->setCurrentCharFormat(textFormat);
    this->mergeCurrentCharFormat(textFormat);
    this->appendPlainText(message);
    this->verticalScrollBar()->setValue(this->verticalScrollBar()->maximum());
}


void EventLog::on_log(const QString &color, const QString &message)
{
    QBrush brush = QBrush(Qt::SolidPattern);
    brush.setColor(QColor(color));

    QString logMessage = message;
    if (m_useTimestamp) {
        QString timestamp = QDateTime::currentDateTime().toString("[yyyyMMdd hh:mm:ss]");
        logMessage = timestamp + " " + logMessage;
    }

    addMessageToBoard(logMessage, brush);
}
