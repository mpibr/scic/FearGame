#ifndef TDTINTERFACE_H
#define TDTINTERFACE_H

#include <iostream>
#include <QDebug>
#include "rpcoxlib.h"

class TDTInterface
{
public:
    explicit TDTInterface(){}

    int connect(const QString &portName, int portId, int deviceId);
    int load(const QString &fileRCX);
    int run();
    int soft();
    void destroy();

    static const int TDT_DEVICE_RP2 = 0xD2;
    static const int TDT_DEVICE_RZ6 = 0xD6;
    static const int TDT_STATUS_NULL = 0x00;
    static const int TDT_STATUS_CONNECTED = 0x01;
    static const int TDT_STATUS_CIRCUIT_LOADED = 0x02;
    static const int TDT_STATUS_CIRCUIT_RUNNING = 0x04;

private:
    RPCOXLib::RPcoX rp;
    HRESULT ht;

    bool checkStatus(int statusQuery);
};

#endif // TDTINTERFACE_H
