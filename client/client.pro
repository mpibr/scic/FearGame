#-------------------------------------------------
#
# Project created by QtCreator 2019-01-15T11:01:07
#
#-------------------------------------------------

QT       += core gui serialport axcontainer network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = client
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    eventlog.cpp \
    serialpackage.cpp \
    serialport.cpp \
    enginedriver.cpp \
    protocol.cpp \
    tdtinterface.cpp \
    eventwriter.cpp \
    socketclient.cpp

HEADERS += \
        mainwindow.h \
    eventlog.h \
    serialpackage.h \
    serialport.h \
    enginedriver.h \
    protocol.h \
    tdtinterface.h \
    eventwriter.h \
    socketclient.h

FORMS += \
        mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    icons.qrc

macx {
    # load demo rpcoxlib
    #INCLUDEPATH += $$PWD/../libs/tdt/macx
    #LIBS += -L$$PWD/../libs/tdt/macx -lrpcox
}



win32 {

    # NOTE: COMPILES UNDER WINDOWS ONLY BECAUSE OF THE
    # ACTIVE-X COM OBJECT TDT REQUIRES!

    # QT automatically generates the classes from the com object
    TYPELIBS = $$system(dumpcpp -getfile {D323A622-1D13-11D4-8858-444553540000})

    isEmpty(TYPELIBS) {
        message("TDT library not found!")
    } else {
        TYPELIBS = $$system(dumpcpp {D323A622-1D13-11D4-8858-444553540000})
        HEADERS += rpcoxlib.h
        SOURCES  += rpcoxlib.cpp
    }
}

