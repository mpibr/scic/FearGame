#ifndef SERIALPACKAGE_H
#define SERIALPACKAGE_H

#include "Arduino.h"

class SerialPackage
{
public:
    explicit SerialPackage();

    static void parse(char *buffer, const char *message);
    static void package(char *buffer, const char *message);
    static bool isSizeModular(const char *buffer);
    static bool isSizeValid(const char *buffer, uint16_t bufferSize);
    static bool isChecksumValid(char *buffer);
    
    static uint8_t checksum(const char *message);

    static char *pack(char *buffer, uint8_t value);
    static char *pack(char *buffer, uint16_t value);
    static char *pack(char *buffer, uint32_t value);

    static const char *unpack(uint8_t *value, const char *buffer);
    static const char *unpack(uint16_t *value, const char *buffer);
    static const char *unpack(uint32_t *value, const char *buffer);
};

#endif /* SERIALPACKAGE_H */
