#ifndef HARDWARELAYOUT_H
#define HARDWARELAYOUT_H

#include "Arduino.h"
#include "digitalpulse.h"

class HardwareLayout
{
public:
    explicit HardwareLayout();
    uint8_t update();
    bool enable(uint8_t mask, uint8_t state);
    bool setTrigger(uint8_t mask, uint32_t time_delay, uint32_t time_up, uint32_t time_down, uint16_t cycles);
    
private:
    // DIGITAL PORTA
    // PA0 - PA7 = Pins 22 - 29
    // PA0 = PIN22 = PIN_TRIG_BASLER
    // PA1 = PIN23 = PIN_TRIG_NVISTA
    // PA2 = PIN24 = PIN_TRIG_TDT
    // PA3 = PIN25 = PIN_TRIG_SHOCK
    // PA4 = PIN26 = PIN_TRIG_STOP
    // PA5 = PIN27 = PIN_SYNC_BASLER
    // PA6 = PIN28 = PIN_SYNC_NVISTA
    // PA7 = PIN29 = EMPTY
    const uint8_t PORT_REGISTER =  B10011111; // DI = 0 (5,6), DO = (0..4, 7)
    
    const uint8_t PIN_TRIG_BASLER = 22;
    const uint8_t PIN_TRIG_NVISTA = 23;
    const uint8_t PIN_TRIG_TDT = 24;
    const uint8_t PIN_TRIG_SHOCK = 25;
    const uint8_t PIN_TRIG_STOP = 26;
    const uint8_t PIN_SYNC_BASLER = 27;
    const uint8_t PIN_SYNC_NVISTA = 28;

    const uint8_t MASK_TRIG_BASLER = 1 << 0;
    const uint8_t MASK_TRIG_NVISTA = 1 << 1;
    const uint8_t MASK_TRIG_TDT =  1 << 2;
    const uint8_t MASK_TRIG_SHOCK = 1 << 3;
    const uint8_t MASK_TRIG_STOP = 1 << 4;
    const uint8_t MASK_SYNC_BASLER = 1 << 5;
    const uint8_t MASK_SYNC_NVISTA = 1 << 6;
    
    uint8_t m_port_state;

    static const uint8_t TRIGGERS_SIZE = 5;
    DigitalPulse m_triggers[TRIGGERS_SIZE];  
};

#endif /* HARDWARELAYOUT_H */
