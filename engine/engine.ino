/*  ARDUINO ENGINE
 *  Project: FearGame
 *  
 *  28 March 2019
 *  Georgi Tushev
 */

#include "serialport.h"
#include "hardwarelayout.h"
#include "eventslayout.h"

const uint16_t buffer_size = 64;
char buffer_message[buffer_size];
char buffer_read[buffer_size];
char buffer_write[buffer_size];

auto srPort = SerialPort(buffer_message, buffer_size);
auto evLayout = EventsLayout(buffer_read, buffer_write, buffer_size);


void setup()
{
    srPort.open(115200);
}


void loop()
{
    if (srPort.poll()) {
        evLayout.dispatch(srPort.message());
    }


    if (evLayout.poll()) {
        srPort.send(evLayout.respond());
    }

    if (evLayout.tick()) {
        srPort.send(evLayout.respond());
    }
    
    delay(1);
}
