#include "hardwarelayout.h"

HardwareLayout::HardwareLayout()
{
    // set port register
    DDRA = PORT_REGISTER;
    PORTA = B00000000;
    
    m_port_state = PINA;

    // set triggers pins
    m_triggers[0].m_pin = PIN_TRIG_BASLER;
    m_triggers[1].m_pin = PIN_TRIG_NVISTA;
    m_triggers[2].m_pin = PIN_TRIG_TDT;
    m_triggers[3].m_pin = PIN_TRIG_SHOCK;
    m_triggers[4].m_pin = PIN_TRIG_STOP;

    // set triggers masks
    m_triggers[0].m_mask = MASK_TRIG_BASLER;
    m_triggers[1].m_mask = MASK_TRIG_NVISTA;
    m_triggers[2].m_mask = MASK_TRIG_TDT;
    m_triggers[3].m_mask = MASK_TRIG_SHOCK;
    m_triggers[4].m_mask = MASK_TRIG_STOP;
}


uint8_t HardwareLayout::update()
{
    // uppdate triggers
    for (uint8_t k = 0; k < TRIGGERS_SIZE; ++k)
        m_triggers[k].update();

    // port state
    uint8_t port_change = PINA;

    // rising edge
    uint8_t state = (m_port_state ^ port_change) & port_change;

    // falling edge
    //uint8_t state = (m_port_state ^ port_change) & m_port_state;

    // update port state to current change
    m_port_state = port_change;

    return state;
}



bool HardwareLayout::enable(uint8_t mask, uint8_t state)
{
    bool status = false;
    for (uint8_t k = 0; k < TRIGGERS_SIZE; ++k) {
        if (m_triggers[k].m_mask & mask) {
            if (state == 0) {
                m_triggers[k].stop();
            }
            else {
                m_triggers[k].start();
            }
            status = true;
        }
    }

    return status;
}



bool HardwareLayout::setTrigger(uint8_t mask, uint32_t time_delay, uint32_t time_up, uint32_t time_down, uint16_t cycles)
{
    int8_t status = false;
    for (uint8_t k = 0; k < TRIGGERS_SIZE; ++k) {
        if (m_triggers[k].m_mask & mask) {
            m_triggers[k].m_delay = time_delay;
            m_triggers[k].m_uptime = time_up;
            m_triggers[k].m_downtime = time_down;
            m_triggers[k].m_cycles = cycles;
            status = true;
        }
    }
    return status;
}
