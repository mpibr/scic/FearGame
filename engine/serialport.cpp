#include "serialport.h"

SerialPort::SerialPort(char *buffer, const uint16_t bufferSize) :
    m_flagReceiving(false),
    m_bufferIndex(0)
{
    m_buffer = buffer;
    m_bufferSize = bufferSize;
}


void SerialPort::open(const uint32_t baudRate)
{
    Serial.begin(baudRate);
}


bool SerialPort::poll()
{
    bool flagReady = false;

    if (Serial.available()) {

        char newChar = static_cast<char>(Serial.read());

        if ((m_flagReceiving == false) && (newChar == '!')) {
            m_flagReceiving = true;
            m_bufferIndex = 0;
        }
        
        if ((m_flagReceiving == true) && 
        (((newChar == '\n') || (newChar == '\r')) || (m_bufferIndex >= (m_bufferSize - 1)))) {
            flagReady = true;
            m_flagReceiving = false;
            m_buffer[m_bufferIndex] = '\0';
        }

        if ((m_flagReceiving == true) &&
        (('0' <= newChar && newChar <= '9') ||
        ('a' <= newChar && newChar <= 'f') ||
        ('A' <= newChar && newChar <= 'F'))) {
            m_buffer[m_bufferIndex++] = toupper(newChar);
        }

    }

    return flagReady;
}


void SerialPort::send(const char *message)
{
    Serial.print('!');
    Serial.println(message);
}


const char *SerialPort::message()
{
    return m_buffer;
}
