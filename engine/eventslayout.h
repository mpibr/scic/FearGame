#ifndef EVENTSLAYOUT_H
#define EVENTSLAYOUT_H

#include "Arduino.h"
#include "serialpackage.h"
#include "hardwarelayout.h"

class EventsLayout
{
public:
    explicit EventsLayout(char *buffer_read, char *buffer_write, const uint16_t buffer_size);
    void dispatch(const char *message);
    const char *respond();
    bool poll();
    bool tick();

    HardwareLayout hwLayout;
    
private:
    bool m_flagReady;
    char *m_buffer_read;
    const char *p_buffer_read;
    char *m_buffer_write;
    char *p_buffer_write;
    uint16_t m_buffer_size;
    uint32_t m_counter;
    uint32_t m_timestamp;

    void event(uint8_t address, uint8_t code);
    void error(uint8_t error_id);
    void action_handshake();
    void action_enable();
    void action_set();
    
    const uint8_t ADDRESS_CLIENT_ENGINE = 0xCE; // incoming
    const uint8_t ADDRESS_ENGINE_CLIENT = 0xEC; // outgoing
    const uint8_t CODE_HANDSHAKE = 0x00;
    const uint8_t CODE_SET = 0x01;
    const uint8_t CODE_ENABLE = 0x02;
    static const uint8_t CODE_SIZE = 0x03;
    const uint8_t CODE_PORT = 0xAA;

    // ERROR HANDLING
    const uint8_t CODE_ERROR = 0xEE;
    const uint8_t ERROR_MESSAGE_TRUNCATED = 0xE0;
    const uint8_t ERROR_INVALID_CHECKSUM = 0xE1;
    const uint8_t ERROR_INVALID_ADDRESS = 0xE2;
    const uint8_t ERROR_INVALID_CODE = 0xE3;
    const uint8_t ERROR_INVALID_CALLBACK = 0xE4;
    const uint8_t ERROR_MESSAGE_ISSHORT = 0xE5;
    const uint8_t ERROR_TRIGGER_SET = 0xE6;
    const uint8_t ERROR_TRIGGER_ENABLE = 0xE7;

    typedef void (EventsLayout::*ActionType)();
    
    ActionType m_actions[CODE_SIZE] = {
        &EventsLayout::action_handshake,
        &EventsLayout::action_set,
        &EventsLayout::action_enable
   };
};

#endif /* EVENTSLAYOUT_H */
