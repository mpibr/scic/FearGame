#include "serialpackage.h"

SerialPackage::SerialPackage()
{
    
}


void SerialPackage::parse(char *buffer, const char *message)
{
    char *p_buffer = buffer;
    while(*message) {

        if ((('0' <= *message) && (*message <= '9')) ||
            (('A' <= *message) && (*message <= 'F')) ||
            (('a' <= *message) && (*message <= 'f'))) {
                *p_buffer++ = *message;
        }
        
        message++;
    }
    *p_buffer = '\0';
}


bool SerialPackage::isSizeModular(const char *buffer)
{
    return (strlen(buffer) % 2) == 0;
}


bool SerialPackage::isSizeValid(const char *buffer, uint16_t bufferSize)
{
    return (strlen(buffer) >= bufferSize);    
}


bool SerialPackage::isChecksumValid(char *buffer)
{
    char *p_buffer = buffer;

    size_t checksumOffset = strlen(p_buffer) - 2;
    p_buffer += checksumOffset;
    
    uint8_t checksumReceived = 0;
    unpack(&checksumReceived, p_buffer);
    p_buffer = '\0';
    
    uint8_t checksumLocal = checksum(buffer);
    
    return (checksumLocal == checksumReceived);
}


void SerialPackage::package(char *buffer, const char *message)
{
    uint8_t checksumValue = checksum(message);
    char *p_buffer = buffer;
    p_buffer = pack(p_buffer, checksumValue);
    *p_buffer = '\0';
}


uint8_t SerialPackage::checksum(const char *message)
{
    
    uint8_t valueChecksum = 0;
    const char *p_message = message;
    while (*p_message) {
        uint8_t value = 0;
        p_message = unpack(&value, p_message);
        valueChecksum += value;
    }

    // calculate two's complemet
    valueChecksum = ~valueChecksum + 1;
    
    return valueChecksum;
}


char *SerialPackage::pack(char *buffer, uint8_t value)
{
    char local_buffer[3];
    char *p_buffer = buffer;
    sprintf(local_buffer, "%02X", value);
    strncpy(p_buffer, local_buffer, 2);
    p_buffer += 2;
    return p_buffer;
}


char *SerialPackage::pack(char *buffer, uint16_t value)
{
    char local_buffer[5];
    char *p_buffer = buffer;
    sprintf(local_buffer, "%04X", value);
    strncpy(p_buffer, local_buffer, 4);
    p_buffer += 4;
    return p_buffer;
}


char *SerialPackage::pack(char *buffer, uint32_t value)
{
    char local_buffer[9];
    char *p_buffer = buffer;
    sprintf(local_buffer, "%08lX", value);
    strncpy(p_buffer, local_buffer, 8);
    p_buffer += 8;
    return p_buffer;
}


const char *SerialPackage::unpack(uint8_t *value, const char *buffer)
{
    const char *p_buffer = buffer;
    char local_buffer[3];
    strncpy(local_buffer, buffer, 2);
    local_buffer[2] = '\0';
    *value = static_cast<uint8_t>(strtoul(local_buffer, NULL, 16));
    p_buffer += 2;
    return p_buffer;
}


const char *SerialPackage::unpack(uint16_t *value, const char *buffer)
{
    const char *p_buffer = buffer;
    char local_buffer[5];
    strncpy(local_buffer, buffer, 4);
    local_buffer[4] = '\0';
    *value = static_cast<uint16_t>(strtoul(local_buffer, NULL, 16));
    p_buffer += 4;
    return p_buffer;
}


const char *SerialPackage::unpack(uint32_t *value, const char *buffer)
{
    const char *p_buffer = buffer;
    char local_buffer[9];
    strncpy(local_buffer, buffer, 8);
    local_buffer[8] = '\0';
    *value = static_cast<uint32_t>(strtoul(local_buffer, NULL, 16));
    p_buffer += 8;
    return p_buffer;
}
