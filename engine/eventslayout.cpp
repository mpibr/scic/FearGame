#include "eventslayout.h"

EventsLayout::EventsLayout(char *buffer_read, char *buffer_write, uint16_t buffer_size) :
    m_flagReady(false),
    m_buffer_read(buffer_read),
    p_buffer_read(buffer_read),
    m_buffer_write(buffer_write),
    p_buffer_write(buffer_write),
    m_buffer_size(buffer_size),
    m_counter(0),
    m_timestamp(0)
{
    
}


void EventsLayout::dispatch(const char *message)
{
    // re-initialize reading pointer
    p_buffer_read = m_buffer_read;

    // parse message
    SerialPackage::parse(m_buffer_read, message);

    if (!SerialPackage::isSizeModular(m_buffer_read)) {
        error(ERROR_MESSAGE_TRUNCATED);
        return;
    }
    
    if (!SerialPackage::isChecksumValid(m_buffer_read)) {
        //error(ERROR_INVALID_CHECKSUM);
        //return;
    }

    // parse address
    uint8_t address = 0;
    p_buffer_read = SerialPackage::unpack(&address, p_buffer_read);
    if (address != ADDRESS_CLIENT_ENGINE) {
        error(ERROR_INVALID_ADDRESS);
        return;
    }
    
    // parse code
    uint8_t code = 0;
    p_buffer_read = SerialPackage::unpack(&code, p_buffer_read);
    if (!(code < CODE_SIZE)) {
        error(ERROR_INVALID_CODE);
        return;
    }

    // callback
    ActionType callback = m_actions[code];
    if (!callback) {
        error(ERROR_INVALID_CALLBACK);
        return;
    }
    (this->*callback)();
}


bool EventsLayout::poll()
{
    bool flagEvent = false;
    
    if (m_flagReady) {
        m_flagReady = false;
        flagEvent = true;
        SerialPackage::package(p_buffer_write, m_buffer_write);
    }

    return flagEvent;
}


bool EventsLayout::tick()
{
    bool flagEvent = false;
    uint8_t port_state = hwLayout.update();
    
    if (port_state > 0) {
        event(ADDRESS_ENGINE_CLIENT, CODE_PORT);
        p_buffer_write = SerialPackage::pack(p_buffer_write, port_state);
        SerialPackage::package(p_buffer_write, m_buffer_write);
        m_flagReady = false;
        flagEvent = true;
    }

    return flagEvent;
}


const char *EventsLayout::respond()
{
    return m_buffer_write;
}



void EventsLayout::event(uint8_t address, uint8_t code)
{
    m_timestamp = millis();
    m_counter++;
    
    memset(m_buffer_write, '\0', m_buffer_size);
    p_buffer_write = m_buffer_write;
    p_buffer_write = SerialPackage::pack(p_buffer_write, address);
    p_buffer_write = SerialPackage::pack(p_buffer_write, code);
    p_buffer_write = SerialPackage::pack(p_buffer_write, m_counter);
    p_buffer_write = SerialPackage::pack(p_buffer_write, m_timestamp);
    m_flagReady = true;
}


void EventsLayout::error(uint8_t error_id)
{
    event(ADDRESS_ENGINE_CLIENT, CODE_ERROR);
    p_buffer_write = SerialPackage::pack(p_buffer_write, error_id);
}


void EventsLayout::action_handshake()
{
    uint16_t value = 0x1234;
    event(ADDRESS_ENGINE_CLIENT, CODE_HANDSHAKE);
    p_buffer_write = SerialPackage::pack(p_buffer_write, value);
}


void EventsLayout::action_set()
{
    uint8_t mask = 0;
    uint32_t time_delay = 0;
    uint32_t time_up = 0;
    uint32_t time_down = 0;
    uint16_t cycles = 0;

    p_buffer_read = SerialPackage::unpack(&mask, p_buffer_read);
    p_buffer_read = SerialPackage::unpack(&time_delay, p_buffer_read);
    p_buffer_read = SerialPackage::unpack(&time_up, p_buffer_read);
    p_buffer_read = SerialPackage::unpack(&time_down, p_buffer_read);
    p_buffer_read = SerialPackage::unpack(&cycles, p_buffer_read);

    if(!hwLayout.setTrigger(mask, time_delay, time_up, time_down, cycles)) {
        error(ERROR_TRIGGER_SET);
        return;
    }
    
    event(ADDRESS_ENGINE_CLIENT, CODE_SET);
    p_buffer_write = SerialPackage::pack(p_buffer_write, mask);
    p_buffer_write = SerialPackage::pack(p_buffer_write, time_delay);
    p_buffer_write = SerialPackage::pack(p_buffer_write, time_up);
    p_buffer_write = SerialPackage::pack(p_buffer_write, time_down);
    p_buffer_write = SerialPackage::pack(p_buffer_write, cycles);
}


void EventsLayout::action_enable()
{
    uint8_t mask = 0;
    uint8_t state = 0;

    if (!SerialPackage::isSizeValid(p_buffer_read, 4)) {
        error(ERROR_MESSAGE_ISSHORT);
        return;
    }

    p_buffer_read = SerialPackage::unpack(&mask, p_buffer_read);
    p_buffer_read = SerialPackage::unpack(&state, p_buffer_read);

    if (!hwLayout.enable(mask, state)) {
        error(ERROR_TRIGGER_ENABLE);
        return;
    }

    event(ADDRESS_ENGINE_CLIENT, CODE_ENABLE);
    p_buffer_write = SerialPackage::pack(p_buffer_write, mask);
    p_buffer_write = SerialPackage::pack(p_buffer_write, state);
}
