#include "serialpackage.h"
#include "serialport.h"
#include "eventloop.h"

// --- DEFINITIONS --- //
/*
struct ISR_Event_t {
    bool flag;
    uint8_t address;
    uint8_t command;
    uint32_t counter;
    uint32_t timestamp;
};

void processSync(char *syncBuffer, ISR_Event_t *syncEvent);
void ISR_ReadSYNC_Basler();
void ISR_ReadSYNC_nVista();


const uint8_t ADDRESS_ENGINE_CLIENT = 0xEC; // outgoing
const uint8_t COMMAND_FRAME_Pylon = 0xAA;
const uint8_t COMMAND_FRAME_nVista = 0xBB;


ISR_Event_t sync_Pylon = {false, ADDRESS_ENGINE_CLIENT, COMMAND_FRAME_Pylon, 0, 0};
ISR_Event_t sync_nVista = {false, ADDRESS_ENGINE_CLIENT, COMMAND_FRAME_nVista, 0, 0};
*/


// --- GLOBAL --- //
const uint16_t BUFFER_SIZE = 128;
char buffer_Serial[BUFFER_SIZE];
char buffer_Event[BUFFER_SIZE];
char buffer_Sync[BUFFER_SIZE];


auto srPort = SerialPort(buffer_Serial, BUFFER_SIZE);
auto evLoop = EventLoop(buffer_Event, BUFFER_SIZE);


void setup()
{
  srPort.open(115200);
  
  //attachInterrupt(digitalPinToInterrupt(evLoop.layout.PIN_SYNC_Basler), ISR_ReadSYNC_Basler, RISING);
  //attachInterrupt(digitalPinToInterrupt(evLoop.layout.PIN_SYNC_nVista), ISR_ReadSYNC_nVista, RISING);
  
}

void loop()
{
  // parse client command
  if (srPort.poll()) {
    
    evLoop.dispatch(srPort.message());
    
  }

  // send respond command
  if (evLoop.process()) {
    
    srPort.send(evLoop.respond());
    
  }

/*
  // check for sync events
  if (sync_Pylon.flag) {
    
    processSync(buffer_Sync, &sync_Pylon);
    
  }

  if (sync_nVista.flag) {
    
    processSync(buffer_Sync, &sync_nVista);
    
  }
*/
  
}



/*
// --- FUNCTIONS --- //
void processSync(char *syncBuffer, ISR_Event_t *syncEvent)
{
    syncEvent->flag = false;
    char *p_syncBuffer = syncBuffer;
    p_syncBuffer = SerialPackage::pack(p_syncBuffer, syncEvent->address);
    p_syncBuffer = SerialPackage::pack(p_syncBuffer, syncEvent->command);
    p_syncBuffer = SerialPackage::pack(p_syncBuffer, syncEvent->counter);
    p_syncBuffer = SerialPackage::pack(p_syncBuffer, syncEvent->timestamp);
    SerialPackage::package(p_syncBuffer, syncBuffer);
    Serial.print('!');
    Serial.println(syncBuffer); 
}

// --- INTERRUPT CALLBACKS --- //

void ISR_ReadSYNC_Basler()
{
  //sync_Pylon.timestamp = millis();
  sync_Pylon.counter++;
  //sync_Pylon.flag = true;
}


void ISR_ReadSYNC_nVista()
{
    //sync_nVista.timestamp = millis();
    sync_nVista.counter++;
    sync_nVista.flag = true;
}



ISR(TIMER1_OVF_vect)
{
  TCNT1 = evLoop.layout.hwTimer_ISRcount;
  //PORTD ^= evLoop.layout.hwTimer_port; // use with Arduino Uno 
  PORTA ^= evLoop.layout.hwTimer_port; // use with Arduino Mega 
}
*/
