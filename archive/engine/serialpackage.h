#ifndef SERIALPACKAGE_H
#define SERIALPACKAGE_H

#include "Arduino.h"

class SerialPackage
{
public:
    explicit SerialPackage();

    static uint8_t parse(char *buffer, const char *message);
    static void package(char *buffer, const char *message);

    static uint8_t checksum(const char *message);

    static char *pack(char *buffer, uint8_t value);
    static char *pack(char *buffer, uint16_t value);
    static char *pack(char *buffer, uint32_t value);

    static const char *unpack(uint8_t &value, const char *buffer);
    static const char *unpack(uint16_t &value, const char *buffer);
    static const char *unpack(uint32_t &value, const char *buffer);

    static const uint8_t STATUS_DONE = 0x00;
    static const uint8_t STATUS_MESSAGE_TRUNCATED = 0xE1;
    static const uint8_t STATUS_INVALID_CHECKSUM = 0xE2;
};

#endif /* SERIALPACKAGE_H */
