#ifndef EVENTLOOP_H
#define EVENTLOOP_H

#include "Arduino.h"
#include "serialpackage.h"
#include "hardwarelayout.h"

class EventLoop
{
public:
    explicit EventLoop(char *buffer, const uint16_t bufferSize);
    void dispatch(const char *message);
    bool process();
    const char *respond();

    HardwareLayout layout;

    bool flagStartEvent;
    bool flagDoneEvent;
    

private:
    bool m_flagReady;
    char *m_buffer;
    const char *p_buffer_read;
    char *p_buffer_write;
    uint16_t m_bufferSize;
    uint32_t m_counter;
    uint32_t m_timestamp;
    
    void action_handshake(void);
    void action_power(void);
    void action_start(void);
    void action_stop(void);
    void action_resume(void);
    void action_pause(void);
    void action_setDelay(void);
    void action_setUptime(void);
    void action_setDowntime(void);
    void action_setCycles(void);
    //void action_timerArm(void);
    //void action_timerEnable(void);
    void action_error(uint8_t errorCode);
    
    typedef void (EventLoop::*ActionType)(void);
    struct ActionMap {
        uint8_t code;
        ActionType callback;
    };

    const uint8_t ADDRESS_CLIENT_ENGINE = 0xCE; // incoming
    const uint8_t ADDRESS_ENGINE_CLIENT = 0xEC; // outgoing
    const uint8_t ADDRESS_ERROR = 0xFF;
    
    const uint8_t CODE_NULL = 0x00;
    const uint8_t CODE_HANDSHAKE = 0x01;
    const uint8_t CODE_POWER = 0x02;
    const uint8_t CODE_START = 0x03;
    const uint8_t CODE_STOP = 0x04;
    const uint8_t CODE_RESUME = 0x05;
    const uint8_t CODE_PAUSE = 0x06;
    const uint8_t CODE_SETDELAY = 0x07;
    const uint8_t CODE_SETUPTIME = 0x08;
    const uint8_t CODE_SETDOWNTIME = 0x09;
    const uint8_t CODE_SETCYCLES = 0x0A;
    //const uint8_t CODE_TIMERARM = 0x0B;
    //const uint8_t CODE_TIMERENABLE = 0x0C;
    static const uint8_t CODE_MAP_SIZE = 0x0B;

    const uint8_t ERROR_INVALID_ADDRESS = 0xE3;
    const uint8_t ERROR_INVALID_CODE = 0xE4;
    const uint8_t ERROR_INVALID_CALLBACK = 0xE5;
    const uint8_t ERROR_INVALID_VALUE = 0xE6;
    
    ActionMap m_actions[CODE_MAP_SIZE] = {
        {CODE_NULL, 0},
        {CODE_HANDSHAKE, &EventLoop::action_handshake},
        {CODE_POWER, &EventLoop::action_power},
        {CODE_START, &EventLoop::action_start},
        {CODE_STOP, &EventLoop::action_stop},
        {CODE_RESUME, &EventLoop::action_resume},
        {CODE_PAUSE, &EventLoop::action_pause},
        {CODE_SETDELAY, &EventLoop::action_setDelay},
        {CODE_SETUPTIME, &EventLoop::action_setUptime},
        {CODE_SETDOWNTIME, &EventLoop::action_setDowntime},
        {CODE_SETCYCLES, &EventLoop::action_setCycles}
        //{CODE_TIMERARM, &EventLoop::action_timerArm},
        //{CODE_TIMERENABLE, &EventLoop::action_timerEnable}
    };

};

#endif /* EVENTLOOP_H */
