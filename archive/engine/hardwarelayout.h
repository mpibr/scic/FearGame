#ifndef HARDWARELAYOUT_H
#define HARDWARELAYOUT_H

#include "Arduino.h"
#include "digitalpulse.h"

class HardwareLayout
{
public:
    explicit HardwareLayout();
    void update();

    void power(uint8_t state);
    void start(uint8_t mask);
    void stop();
    void resume();
    void pause();
    
    //void hwTimerArm(double frequency);
    //void hwTimerEnable(bool state);
    void setDelay(uint8_t pin, uint32_t value);
    void setUptime(uint8_t pin, uint32_t value);
    void setDowntime(uint8_t pin, uint32_t value);
    void setCycles(uint8_t pin, uint32_t value);

    //const byte hwTimer_port = (1 << 0); // Arduino Pin 22 is line 0 of PORTA
    //uint32_t hwTimer_ISRcount = 0;

    // pins are fixed for the model
    const uint8_t PIN_SYNC_Basler = 2;
    const uint8_t PIN_SYNC_nVista = 3;
    const uint8_t PIN_TRIG_Basler = 22; // digital Pin 22, PORTA Arduino Mega, B00000001
    const uint8_t PIN_TRIG_nVista = 23;
    const uint8_t PIN_TRIG_TDT = 24;
    const uint8_t PIN_TRIG_SHOCK = 25;
    const uint8_t PIN_VLTG_Basler = 36;
    const uint8_t PIN_STOP_Basler = 37;
    static const uint8_t TRIAL_TRIGGERS_SIZE = 4;

    DigitalPulse m_triggerStop;
    DigitalPulse m_triggerTrial[TRIAL_TRIGGERS_SIZE];
};


#endif /* HARDWARELAYOUT_H */
