#include "eventloop.h"

EventLoop::EventLoop(char *buffer, const uint16_t bufferSize) :
    flagStartEvent(false),
    flagDoneEvent(false),
    m_flagReady(false),
    m_buffer(nullptr),
    p_buffer_read(nullptr),
    p_buffer_write(nullptr),
    m_bufferSize(0),
    m_counter(0),
    m_timestamp(0)
{
    m_buffer = buffer;
    m_bufferSize = bufferSize;
}


void EventLoop::dispatch(const char *message)
{
   
    // re-initialize buffer read/write pointers
    p_buffer_read = m_buffer;
    p_buffer_write = m_buffer;
    
    // update counter and timestamp
    m_counter++;
    m_timestamp = millis();

    // parse message
    uint8_t status = SerialPackage::parse(p_buffer_write, message);
    if (status != SerialPackage::STATUS_DONE) {
        action_error(status);
        return;
    }
    
    // parse address
    uint8_t address = 0;
    p_buffer_read = SerialPackage::unpack(address, p_buffer_read);
    if (address != ADDRESS_CLIENT_ENGINE) {
        action_error(ERROR_INVALID_ADDRESS);
        return;
    }
    p_buffer_write = SerialPackage::pack(p_buffer_write, ADDRESS_ENGINE_CLIENT);

    // parse code
    uint8_t code = 0;
    p_buffer_read = SerialPackage::unpack(code, p_buffer_read);
    
    if ((code == CODE_NULL) || (code >= CODE_MAP_SIZE)) {
        action_error(ERROR_INVALID_CODE);
        return;
    }

    ActionMap *iter = m_actions + code;
    if (!iter) {
        action_error(ERROR_INVALID_CALLBACK);
        return;
    }

    // execute callback
    p_buffer_write += 2;
    (this->*iter->callback)();

    // pack counter and timestamp
    p_buffer_write = SerialPackage::pack(p_buffer_write, m_counter);
    p_buffer_write = SerialPackage::pack(p_buffer_write, m_timestamp);

    // dispatch ready
    m_flagReady = true;
}


bool EventLoop::process()
{
    bool flagEvent = false;

    layout.update();

    if (m_flagReady) {
        m_flagReady = false;   
        flagEvent = true;
        SerialPackage::package(p_buffer_write, m_buffer);
    }
    
    return flagEvent;
}


const char *EventLoop::respond()
{
    return m_buffer;
}


void EventLoop::action_handshake(void)
{
    uint16_t value = 0x1234;
    p_buffer_write = SerialPackage::pack(p_buffer_write, value);
    //Serial.println("HANDSHAKE");
}


void EventLoop::action_power(void)
{
    uint8_t state = 0;
    p_buffer_read = SerialPackage::unpack(state, p_buffer_read);
    p_buffer_write = SerialPackage::pack(p_buffer_write, state);
    layout.power(state);
    //Serial.println("POWER");
}


void EventLoop::action_start(void)
{
    uint8_t mask = 0;
    p_buffer_read = SerialPackage::unpack(mask, p_buffer_read);
    layout.start(mask);
    //Serial.println("START");
}


void EventLoop::action_stop(void)
{
    layout.stop();
    //Serial.println("STOP");
}


void EventLoop::action_resume(void)
{
    layout.resume();
    //Serial.println("RESUME");
}


void EventLoop::action_pause(void)
{
    layout.pause();
    //Serial.println("PAUSE");
}


void EventLoop::action_setDelay(void)
{
    uint8_t pin = 0;
    uint32_t value = 0;
    p_buffer_read = SerialPackage::unpack(pin, p_buffer_read);
    p_buffer_read = SerialPackage::unpack(value, p_buffer_read);
    layout.setDelay(pin, value);
    //Serial.println("DELAY");
}


void EventLoop::action_setUptime(void)
{
    uint8_t pin = 0;
    uint32_t value = 0;
    p_buffer_read = SerialPackage::unpack(pin, p_buffer_read);
    p_buffer_read = SerialPackage::unpack(value, p_buffer_read);
    layout.setUptime(pin, value);
    //Serial.println("SETUPTIME");
}


void EventLoop::action_setDowntime(void)
{
    uint8_t pin = 0;
    uint32_t value = 0;
    p_buffer_read = SerialPackage::unpack(pin, p_buffer_read);
    p_buffer_read = SerialPackage::unpack(value, p_buffer_read);
    layout.setDowntime(pin, value);
    //Serial.println("SETDOWNTIME");
}


void EventLoop::action_setCycles(void)
{
    uint8_t pin = 0;
    uint32_t value = 0;
    p_buffer_read = SerialPackage::unpack(pin, p_buffer_read);
    p_buffer_read = SerialPackage::unpack(value, p_buffer_read);
    layout.setCycles(pin, value);
    //Serial.println("SETCYCLES");
}

/*
void EventLoop::action_timerArm(void)
{
    uint32_t value = 0;
    p_buffer_read = SerialPackage::unpack(value, p_buffer_read);
    layout.hwTimerArm(static_cast<double>(value));
    //Serial.println("TIMERARM");
}


void EventLoop::action_timerEnable(void)
{
    uint8_t value = 0;
    p_buffer_read = SerialPackage::unpack(value, p_buffer_read);
    layout.hwTimerEnable(value > 0);
    flagStartEvent = value > 0;
    flagDoneEvent = value == 0;
    //Serial.println("TIMERENABLE");
}
*/

void EventLoop::action_error(uint8_t errorCode)
{
    p_buffer_write = m_buffer;
    p_buffer_write = SerialPackage::pack(p_buffer_write, ADDRESS_ERROR);
    p_buffer_write = SerialPackage::pack(p_buffer_write, errorCode);
    m_flagReady = true;
}
