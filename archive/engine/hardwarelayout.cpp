#include "hardwarelayout.h"

HardwareLayout::HardwareLayout()
{
    // set pins
    pinMode(PIN_SYNC_Basler, INPUT);
    pinMode(PIN_SYNC_nVista, INPUT);
    pinMode(PIN_TRIG_Basler, OUTPUT); digitalWrite(PIN_TRIG_Basler, LOW);
    pinMode(PIN_TRIG_nVista, OUTPUT); digitalWrite(PIN_TRIG_nVista, LOW);
    pinMode(PIN_TRIG_TDT, OUTPUT); digitalWrite(PIN_TRIG_TDT, LOW);
    pinMode(PIN_TRIG_SHOCK, OUTPUT); digitalWrite(PIN_TRIG_SHOCK, LOW);
    pinMode(PIN_VLTG_Basler, OUTPUT); digitalWrite(PIN_VLTG_Basler, LOW);
    pinMode(PIN_STOP_Basler, OUTPUT); digitalWrite(PIN_STOP_Basler, LOW);

    // set digital triggers
    m_triggerStop.m_pin = PIN_STOP_Basler;
    m_triggerTrial[0].m_pin = PIN_TRIG_Basler;
    m_triggerTrial[1].m_pin = PIN_TRIG_nVista;
    m_triggerTrial[2].m_pin = PIN_TRIG_TDT;
    m_triggerTrial[3].m_pin = PIN_TRIG_SHOCK;
}


void HardwareLayout::update()
{
    m_triggerStop.update();

    for (uint8_t k = 0; k < TRIAL_TRIGGERS_SIZE; k++)
        m_triggerTrial[k].update();
}


void HardwareLayout::power(uint8_t state)
{
    uint8_t dostate = (state > 0) ? HIGH : LOW;
    digitalWrite(PIN_VLTG_Basler, dostate);
}


void HardwareLayout::start(uint8_t mask)
{
    // start triggers based on trial mask
    for (uint8_t k = 0; k < TRIAL_TRIGGERS_SIZE; k++) {
        if (bitRead(mask, TRIAL_TRIGGERS_SIZE - k - 1))
            m_triggerTrial[k].start();
    }
}


void HardwareLayout::stop()
{
    // stop triggers
    for (uint8_t k = 0; k < TRIAL_TRIGGERS_SIZE; k++) {
        m_triggerTrial[k].stop();
    }

    // camera stop signal
    m_triggerStop.start();
}


void HardwareLayout::resume()
{
    for (uint8_t k = 0; k < TRIAL_TRIGGERS_SIZE; k++) {
        m_triggerTrial[k].resume();
    }
}


void HardwareLayout::pause()
{
    for (uint8_t k = 0; k < TRIAL_TRIGGERS_SIZE; k++) {
        m_triggerTrial[k].pause();
    }
}


void HardwareLayout::setDelay(uint8_t pin, uint32_t value)
{
    for (uint8_t k = 0; k < TRIAL_TRIGGERS_SIZE; k++) {
        if (m_triggerTrial[k].m_pin == pin)
            m_triggerTrial[k].m_delay = value;
    }
}


void HardwareLayout::setUptime(uint8_t pin, uint32_t value)
{
    for (uint8_t k = 0; k < TRIAL_TRIGGERS_SIZE; k++) {
        if (m_triggerTrial[k].m_pin == pin)
            m_triggerTrial[k].m_uptime = value;
    }
}


void HardwareLayout::setDowntime(uint8_t pin, uint32_t value)
{
    for (uint8_t k = 0; k < TRIAL_TRIGGERS_SIZE; k++) {
        if (m_triggerTrial[k].m_pin == pin)
            m_triggerTrial[k].m_downtime = value;
    }
}


void HardwareLayout::setCycles(uint8_t pin, uint32_t value)
{
    for (uint8_t k = 0; k < TRIAL_TRIGGERS_SIZE; k++) {
        if (m_triggerTrial[k].m_pin == pin)
            m_triggerTrial[k].m_cycles = value;
    }
}


/*
void HardwareLayout::hwTimerArm(double frequency)
{
    const double cpu_freq = 16000000.0;
    const double pre_scale = 256.0;
    const double overflow_count = 65536.0;
    hwTimer_ISRcount = overflow_count - (cpu_freq / pre_scale / (frequency * 2));
    //hwTimer_ISRcount = overflow_count - (cpu_freq / pre_scale / frequency);

    //Serial.print("ARM Timer ");
    //Serial.print(hwTimer_port);
    //Serial.print(" ");
    //Serial.print(hwTimer_ISRcount);
    //Serial.print(" ");
    //Serial.println(frequency);
    
    // arm hardware timer
    noInterrupts();
    TCCR1A = 0;
    TCCR1B = 0;
    TCNT1 = hwTimer_ISRcount; // initialize timer with calculated value
    TCCR1B |= (1 << CS12); // set 256 as a prescale-value
    //TIMSK1 |= (1 << TOIE1); // activate timer overflow interrupt
    interrupts();
}


void HardwareLayout::hwTimerEnable(bool state)
{
  noInterrupts();
  if (state) {
    TIMSK1 |= (1 << TOIE1);
  }
  else {
    TIMSK1 &= ~(1 << TOIE1);
    digitalWrite(PIN_TRIG_Basler, LOW);
  }
    
  interrupts();
}
*/
