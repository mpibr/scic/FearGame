## Arduino Mega ATmega 2560

### 16 Pins BNC Box Layout

|Arduino Pin|Name           |Note     |BNC Pin|
|:---------:|:-------------:|:-------:|:-----:|
|2          |PE4 (OC3B/INT4)|Interrupt|1      |
|3          |PE5 (OC3C/INT5)|Interrupt|2      |
|18         |PDR (TXD1/INT3)|Interrupt|3      |
|19         |PD2 (RXDI/INT2)|Interrupt|4      |
|20         |PD1 (SDA/INT1) |Interrupt|5      |
|21         |PDO (SCL/INT0) |Interrupt|6      |
|37         |PC0 (A8)       |DO       |7      |
|-          |Voltage        |5V       |8      |
|22         |PA0 (AD0)      |DO       |9      |
|23         |PA1 (AD1)      |DO       |10     |
|24         |PA2 (AD2)      |DO       |11     |
|25         |PA3 (AD3)      |DO       |12     |
|26         |PA4 (AD4)      |DO       |13     |
|27         |PA5 (AD5)      |DO       |14     |
|28         |PA6 (AD6)      |DO       |15     |
|29         |PA7 (AD7)      |DO       |16     |

23 April 2019
