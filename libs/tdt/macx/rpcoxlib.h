#ifndef RPCOXLIB_H
#define RPCOXLIB_H

#include <iostream>

typedef void *HRESULT;

namespace RPCOXLib {

class RPcoX {

public:
    explicit RPcoX():status(STATUS_NULL){}
    int ConnectRP2(char interface[], int id);
    void LoadCOF(char circuitName[]);
    long int GetStatus();
    void Halt();
    void ClearCOF();
    void Run();
    void clear();

private:
    long int status;

    static const long int STATUS_NULL = 0x00;
    static const long int STATUS_CONNECTED = 0x01;
    static const long int STATUS_CIRCUIT_LOADED = 0x02;
    static const long int STATUS_CURCUIT_RUNNING = 0x03;
};

}

#endif /* RPCOXLIB_H */