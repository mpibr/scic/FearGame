#include "rpcoxlib.h"

int RPCOXLib::RPcoX::ConnectRP2(char interface[], int id)
{
    printf("RPcoX::ConnectRP2 via %s @ %d\n", interface, id);
    status = STATUS_CONNECTED;
    return 0;
}


void RPCOXLib::RPcoX::LoadCOF(char circuitName[])
{
    printf("RPcoX::LoadCOF %s\n", circuitName);
    status |= STATUS_CIRCUIT_LOADED;
}



long int RPCOXLib::RPcoX::GetStatus()
{
    return status;
}


void RPCOXLib::RPcoX::Halt()
{
    status &= !STATUS_CURCUIT_RUNNING;
}


void RPCOXLib::RPcoX::ClearCOF()
{
    status &= !STATUS_CIRCUIT_LOADED;
}


void RPCOXLib::RPcoX::Run()
{
    status |= STATUS_CURCUIT_RUNNING;
}


void RPCOXLib::RPcoX::clear()
{
    status = STATUS_NULL;
}